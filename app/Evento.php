<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evento extends Model
{
    protected $fillable = ['colaborador_id','portal_id','data'];
    protected $guarded = ['id', 'created_at', 'update_at'];
    protected $table = 'eventos';
}
