<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class StatusLiked implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $colaborador_id;

    public $portal_id;

    /**
     * Create a new event instance.
     *
     * @return void
     */

    public function __construct($colaborador, $portal)
    {
        $this->colaborador_id = $colaborador;
        $this->portal_id  = $portal;


        return response()->json(['colaborador_id' => $colaborador,
                'portal_id' => $portal]);
    }

    // public function __construct($colaborador_id, $portal_id)
    // {
    //     $this->colaborador_id = $colaborador_id;
    //     $this->portal_id  = $portal_id;

    //     return ['colaborador_id' => $colaborador_id,
    //             'portal_id' => $portal_id];
    // }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {      

        return new Channel('status-liked');
    }


}
