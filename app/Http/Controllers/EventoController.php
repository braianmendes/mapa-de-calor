<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Evento;
use App\Events\StatusLiked;


class EventoController extends Controller
{
    public function Passagem(Request $Request){
    	$evento = new Evento();

    	$evento->colaborador_id = $Request->colaborador_id;
    	$evento->portal_id = $Request->portal_id;
    	$evento->data = $Request->data;
    	$this->store($evento);

    	$colaborador = $evento->colaborador_id;
    	$portal = $evento->portal_id;

    	event(new StatusLiked($colaborador, $portal));
    }

    public function store(Evento $st){
    $st->save();
    }    
}
