<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/novo', function () {
	return view ('form');
    // event(new App\Events\StatusLiked('Guest'));
    // return "Event has been sent!";
});

Route::post('/novo', 'EventoController@Passagem');

// Route::post('/novo', function (){

// 	$colaborador_id = request()->colaborador_id;
// 	$portal_id = request()->portal_id;

//      event(new App\Events\StatusLiked($colaborador_id, $portal_id));
//      return "Event has been sent!";
// });

Route::get('/', function () {
    return view('welcome');
});
