<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>GRID</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/grid.css') }}" >
  </head>
<body>

    <header></header>

    <main class="grid">
      <div class="column1" id="column1"></div>
      <div class="column1" id="column2"></div>
      <div class="column1" id="column3"></div>
      <div class="column2" id="column4"></div>
      <div class="column2" id="column5"></div>
      <div class="column2" id="column6"></div>

    </main>



<!-- SCRIPTS -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="//js.pusher.com/3.1/pusher.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <script type="text/javascript">

      // Enable pusher logging - don't include this in production
      Pusher.logToConsole = true;

      var pusher = new Pusher('151c30d47ea025e3a699', {
        encrypted: true
      });

      // Subscribe to the channel we specified in our Laravel Event
      var channel = pusher.subscribe('status-liked');

      // Bind a function to a Event (the full Laravel class)
      channel.bind('App\\Events\\StatusLiked', function(data){

        //var objeto = JSON.stringify(data);
        //alert(JSON.stringify(data));
        var portal_id = data.portal_id;
        var colaborador_id = data.colaborador_id;

        var portal1 = document.getElementById('column1');
        var portal2 = document.getElementById('column2');
        var portal3 = document.getElementById('column3');                
        var portal4 = document.getElementById('column4');        
        var portal5 = document.getElementById('column5');
        var portal6 = document.getElementById('column6');  

        var icon1 = document.createElement('div');
        icon1.className = "fun fun1";
        icon1.id = "icon1";

        var icon2 = document.createElement('div');
        icon2.className = "fun fun2";
        icon2.id = "icon2";      

        var icon3 = document.createElement('div');
        icon3.className = "fun fun3"; 
        icon3.id = "icon3";

        switch (colaborador_id){
          case '1': 
            if (document.getElementById('icon1') != null){
            document.getElementById('icon1').remove();
            } break;
          case '2': 
            if (document.getElementById('icon2') != null){
            document.getElementById('icon2').remove();
            } break;
          case '3': 
            if (document.getElementById('icon3') != null){
            document.getElementById('icon3').remove();
            } break;            
          default: break;
        };        
     
        switch (portal_id){
          case '1':
            switch (colaborador_id){
              case '1':
              portal1.append(icon1); break;
              case '2':
              portal1.append(icon2); break;
              case '3':
              portal1.append(icon3); break;
              default: break;
             }
            break; 
          case '2':
            switch (colaborador_id){
              case '1':
              portal2.append(icon1); break;
              case '2':
              portal2.append(icon2); break;
              case '3':
              portal2.append(icon3); break;
              default: break;
             }
            break;          
          case '3':
            switch (colaborador_id){
              case '1':
              portal3.append(icon1); break;
              case '2':
              portal3.append(icon2); break;
              case '3':
              portal3.append(icon3); break;
              default: break;
             }
            break;                       
          case '4':
            switch (colaborador_id){
              case '1':
              portal4.append(icon1); break;
              case '2':
              portal4.append(icon2); break;
              case '3':
              portal3.append(icon3); break;
              default: break;
             }
            break;                       
          case '5':
            switch (colaborador_id){
              case '1':
              portal5.append(icon1); break;
              case '2':
              portal5.append(icon2); break;
              case '3':
              portal5.append(icon3); break;
              default: break;
             }
            break;                       
          case '6':
            switch (colaborador_id){
              case '1':
              portal6.append(icon1); break;
              case '2':
              portal6.append(icon2); break;
              case '3':
              portal6.append(icon3); break;
              default: break;
             }
            break;                             
        };

      });

    </script>
</body>    
</html>
